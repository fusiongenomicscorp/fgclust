// FgClust - Fast and Scalable Sequence Clustering Algorithm
// Copyright (c) 2017-2018 Fusion Genomics Corp.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <omp.h>
#include <stdio.h>

int main(const int argc, const char *const *const argv) {
    int result = 0;
    #pragma omp parallel for
    for (int i = 0; i < 1000*1000*1000; i++) {
        if (i%(1000*1000) == 0) { printf("%d\n", i/(1000*1000)); }
        for (int j = 0; j < 1000; j++) {
            result = result * 3 + i * j;
        }
    }
    printf("Dummyresult=%d\n", result);
}

