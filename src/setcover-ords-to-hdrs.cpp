// FgClust - Fast and Scalable Sequence Clustering Algorithm
// Copyright (c) 2017-2018 Fusion Genomics Corp.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "kseq.h"

#include <cassert>
#include <iostream>
#include <set>
#include <sstream>
#include <tuple>
#include <vector>

#include <unistd.h>

KSEQ_INIT(int, read)

int main(int argc, char **argv) {
    std::cerr << "GITCOMMIT = " << GITCOMMIT << std::endl;
    std::cerr << "CXXVERSION = " << CXXVERSION << std::endl;

    std::vector<std::tuple<std::string, std::string>> fastarecords;
    
    FILE *fastafile = fopen(argv[1], "r");
    kseq_t *kseq = kseq_init(fileno(fastafile));
    while ( kseq_read(kseq) >= 0 ) { 
        fastarecords.push_back(std::make_tuple(kseq->name.s, kseq->comment.s));
    }
    kseq_destroy(kseq);
    fclose(fastafile);
    
    std::string line;
    int dec = 1;
    for (size_t i = 0; i < fastarecords.size(); i++) {
        std::getline(std::cin, line);
        std::stringstream ss(line);
        int inner, outer, sim;
        ss >> inner; //assert(inner > 0);
        ss >> outer; //assert(outer > 0);
        ss >> sim;
        if (0 == outer) {
            dec = 0;
        }
        inner -= dec;
        outer -= dec;
        assert(inner >= 0);
        assert(outer >= 0);
        assert(sim > 0);
        std::cout << std::get<0>(fastarecords.at(inner)) << "\t" << std::get<0>(fastarecords.at(outer)) << "\t" << sim << "\t" << std::get<1>(fastarecords.at(outer)) << std::endl;
    }
}

